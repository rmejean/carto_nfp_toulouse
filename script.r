###################################################################
# Chargement des packages
###################################################################


library(tidyverse) 
library(data.table)
library(leaflet)
library(sf)
library(mapsf)
library(maptiles)
library(tmap)


##############################
# TRAITEMENT FICHIER RESULTATS
##############################

# import du fichier source (https://data.toulouse-metropole.fr/explore/dataset/legislatives-2024-1t-resultats_old/export/?sort=sequence)
leg_24_1t <- read.csv('data/leg_24_1t.csv', sep =",")

# import de la liste des candidats
leg_24_1t_candidats <- read.csv('data/leg_24_1t_candidats.csv', sep =",")

# traitements fichier source

leg_24_1t <- leg_24_1t |> mutate (circonscription = paste0("0",circonscription)) |>
 mutate (cle_resultatsA = paste0(circonscription, "-00", code.dépôt.du.candidat.A)) |>
 mutate (cle_resultatsB = paste0(circonscription, "-00", code.dépôt.du.candidat.B)) |>
 mutate (cle_resultatsC = paste0(circonscription, "-00", code.dépôt.du.candidat.C)) |>
 mutate (cle_resultatsD = paste0(circonscription, "-00", code.dépôt.du.candidat.D)) |>
 mutate (cle_resultatsE = paste0(circonscription, "-00", code.dépôt.du.candidat.E)) |>
 mutate (cle_resultatsF = paste0(circonscription, "-00", code.dépôt.du.candidat.F)) |>
 mutate (cle_resultatsG = paste0(circonscription, "-00", code.dépôt.du.candidat.G))
 # mutate (cle_resultatsH = paste0(circonscription, "-00", code.dépôt.du.candidat.H)) |>
 # mutate (cle_resultatsI = paste0(circonscription, "-0", Code.dépôt.du.candidat.I)) |>
 # mutate (cle_resultatsJ = paste0(circonscription, "-0", Code.dépôt.du.candidat.J)) |>
 # mutate (cle_resultatsK = paste0(circonscription, "-0", code.dépôt.du.candidat.K))

# conserver uniquement les résultats NFP (inutile ?)

candidNFP <- read.csv('data/candidNFP.csv')
codes_candidats_NFP <- candidNFP[[1]]

# fichier de résultats propre

res_NFP <- leg_24_1t |> filter (cle_resultatsA %in% codes_candidats_NFP | cle_resultatsB %in% codes_candidats_NFP | cle_resultatsC %in% codes_candidats_NFP |
                                  cle_resultatsD %in% codes_candidats_NFP | cle_resultatsE %in% codes_candidats_NFP | cle_resultatsF %in% codes_candidats_NFP |
                                  cle_resultatsG %in% codes_candidats_NFP)


res_NFP <- res_NFP |> mutate(voix_NFP = case_when(cle_resultatsA %in% codes_candidats_NFP ~ nombre.de.voix.du.candidat.A,
                                                  cle_resultatsB %in% codes_candidats_NFP ~ nombre.de.voix.du.candidat.B,
                                                  cle_resultatsC %in% codes_candidats_NFP ~ nombre.de.voix.du.candidat.C,
                                                  cle_resultatsD %in% codes_candidats_NFP ~ nombre.de.voix.du.candidat.D,
                                                  cle_resultatsE %in% codes_candidats_NFP ~ nombre.de.voix.du.candidat.E,
                                                  cle_resultatsF %in% codes_candidats_NFP ~ nombre.de.voix.du.candidat.F,
                                                  cle_resultatsG %in% codes_candidats_NFP ~ nombre.de.voix.du.candidat.G))

# calcul du %

res_NFP <- res_NFP |> select (numéro.du.bureau, canton, circonscription, inscrits, abstentions, votants, votants_émargement, exprimés, blancs, nuls, voix_NFP) |>
  mutate(part_NFP = voix_NFP/exprimés*100) |> rename("bv" = "numéro.du.bureau")


##############################
# CARTOGRAPHIE
##############################

bv_secteurs <- st_read("data/bv_secteurs.gpkg")
bv_secteurs <- bv_secteurs |> select (BV, adresse, nom)

circos <- st_read("data/circos.gpkg")

TOULOUSE_NFP <- merge(
  x = bv_secteurs,
  y = res_NFP,
  by.x = "BV",  # identifiant dans x
  by.y = "bv",  # identifiant dans y
  all.x = TRUE         # conserver toutes les lignes
)

# supprimer le bureau fantome
TOULOUSE_NFP <- TOULOUSE_NFP[-1,]


carte_NFP <- tmap::tm_shape(TOULOUSE_NFP, name = "Résultats NFP à Toulouse") +
  tmap::tm_fill(
    col = "part_NFP",
    textNA="Aucun",
    colorNA="white",
    style="jenks",
    id = "part_NFP",
    palette = "Reds",
    alpha = 0.7,
    legend.reverse = TRUE,
    popup.vars = c(
      "Numéro du BV :" = "BV",
      "Nom du lieu de vote :" = "nom",
      "Nombre de voix :" = "voix_NFP",
      "Part en % :" = "part_NFP")) +
  tm_borders("white", lwd = 1) +
  tmap::tm_shape(circos, name = "Circonscriptions toulousaines") +
  tm_borders("black", lwd = 2) +
  tm_text(text = "code_circo", fontfamily = "sans") +
  tmap::tm_scale_bar() +
  tmap::tmap_mode("view")

carte_NFP
